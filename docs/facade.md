<div align="center">
  <h1>Facade</h1>
</div>

<div align="center">
  <img src="facade_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Facade is a structural pattern that simplifies the interface to a complex set of classes.**

### Real-World Analogy

_Ordering by phone._

The phone operator (facade) offers a simple voice interface to the ordering system, payment gateways,
and various delivery options (subsystem).

### Participants

- :man: **Facade**
    - Maintains references to the subsystem objects.
    - Provides an implementation for:
        - `highLevelOperation`: delegates request to one or more subsystem objects.

- :man: **Subsystem Classes**
    - Provides an implementation for:
        - `lowLevelOperation`: handles the work assigned by the Facade object.

### Collaborations

Clients communicate with the **Subsystem** by sending requests to **Facade**, which forwards them to the appropriate
subsystem object(s).

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When clients need a simple high-level interface to a complex subsystem. Or, when clients should
be decoupled from the subsystem.**

### Motivation

- How do we hide all the low-level complexity of a subsystem to clients who only need some simple high-level
  functionality?
- How do we shield the client from getting coupled to all the classes in a subsystem it needs to fulfill a request?
- How do we structure a subsystem into layers?

### Known Uses

- Simplifying third party APIs:
    - E.g., In a messaging system with various protocols and message formats, a facade can provide a simplified
      interface for sending and receiving messages.
- Integration of Legacy Systems:
    - Facade can be used to interact with the old system through a more modern interface.
- Order processing:
    - The client only needs to call process_order on the facade, which delegates the task to inventory management,
      payment processing, and shipping
- Financial transaction:
    - Simplify making financial transactions delegating to different banking services such as transfers, deposits,
      loans, and account management
- .NET Entity Framework:
    - Entity Framework, a data access technology provides a high-level API for querying and updating the database
      without requiring developers to deal with low-level details.
- Graphics Libraries:
    - Graphics libraries like DirectX or OpenGL use the Facade pattern to provide a simplified interface
      for rendering graphics and interacting with GPU hardware.

### Categorization

Purpose:  **Structural**  
Scope:    **Object**   
Mechanisms: **Composition**

Structural patterns are concerned with how classes and objects are composed to form larger structures.

Structural object patterns describe ways to compose objects to realize new functionality.

### Aspects that can vary

- Interface to a subsystem.

### Solution to causes of redesign

- Tight coupling.
    - Hard to understand: a lot of context needs to be known to understand a part of the system.
    - Hard to change: changing one class necessitates changing many other classes.
    - Hard to reuse in isolation: because classes depend on each other.

### Consequences

| Advantages                                                                                                                                                                           | Disadvantages                                                                                                                                                                                         |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Simplified Interface.** <br> Results in more readable and manageable client code and reduces the learning curve for developers using the subsystems             | :x: **Risk of becoming a god object.** <br> A facade can become coupled to many subsystems, making it very complex and hard to maintain.                                                              |
| :heavy_check_mark: **Decoupling.** <br> Clients are shielded from the details of the subsystem’s implementation. Changes to the Subsystem are less likely to affect the Client code. | :x: **Dependency on the Facade.** <br> Clients that are tightly coupled to the facade may limit the ability to swap out Subsystem components or make changes to the system without affecting clients. |
| :heavy_check_mark: **Provide both ease of use and generality.** <br> Doesn't prevent applications from using subsystem classes if they need to.                                      |                                                                                                                                                                                                       |

### Relations with Other Patterns

_Distinction from other patterns:_

- Mediator is similar to Facade in that it abstracts functionality of existing classes.
    - Mediator often centralizes functionality that doesn't belong in any one of it's colleagues. And a mediator's
      colleagues are aware of and communicate with the mediator instead of communicating with each other directly.
    - A facade merely abstracts the interface to subsystem objects to make them easier to use; it
      doesn't define new functionality. And subsystem classes don't know about it.
- An adapter that holds two or more adaptees looks similar to a facade.
    - The intent of the Adapter Pattern is to alter an interface so that it matches one a client is expecting
    - The intent of the Facade Pattern is to provide a simplified interface to a subsystem.
- Facade is similar to Proxy in that both buffer a complex entity and initialize it on its own.
    - Unlike Facade, Proxy has the same interface as its service object, which makes them interchangeable.

_Combination with other patterns:_

- An abstract factory can be used to enforce the use of a facade and to shield subsystem object creation from the
  clients.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **A facade class that has a simple interface, and delegates most of the work to other classes.**

### Structure

```mermaid
classDiagram
    class Facade {
        - subsystem1: Subsystem1
        - subsystem2: Subsystem2
        - subsystem3: Subsystem3
        + highLevelOperation(): void
    }

    class Subsystem1 {
        + lowLevelOperation(): void
    }

    class Subsystem2 {
        + lowLevelOperation(): void
    }

    class Subsystem3 {
        + lowLevelOperation(): void
    }

    Facade *-- Subsystem1: composes
    Facade *-- Subsystem2: composes
    Facade *-- Subsystem3: composes
```

### Variations

_Abstract versus Configurable facade:_

- **Abstract**: abstract facade that has different implementations.
    - :heavy_check_mark: Reduces client-subsystem coupling by communicating only through an abstract interface.
- **Configurable**: configure facade with different subsystem objects.
    - :heavy_check_mark: Flexible approach to vary behavior

_Public versus private subsystem classes_

- **Public**: subsystem classes public and accessible through facade
    - :heavy_check_mark: Flexible: clients can use simple facade interface and subsystem classes directly
- **Private**: configure sybsystem classes only accessible through facade because they're private
    - :heavy_check_mark: It's enforced that the client isn't directly coupled to the subsystem classes.

### Implementation

In the example we apply the facade pattern to create a video conversion tool.
The facade allows us to convert a video with a simple command: ```converter.to_mp4("my_video.avi")```
Shielding the client from creating low level subsystem objects like: the audio and video streams, the codec and the
container, bitrate, and colorspace

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Facade](https://refactoring.guru/design-patterns/facade)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [pentalog: facade-design-pattern](https://www.pentalog.com/blog/design-patterns/facade-design-pattern/)
- [ArjanCodes: How To Reduce Coupling With Facade | Design Pattern Tutorial](https://youtu.be/jjoLejA4iAE?si=R6RxTtzetll5PQj_)
- [Geekific: The Facade Pattern Explained and Implemented in Java](https://youtu.be/xWk6jvqyhAQ?si=bWrMejQ2aE8ACSkI)

<br>
<br>
