<h1 style="text-align: center;">Facade App</h1>

## Theory

For a description of the Facade design pattern, you can refer
to the: [Facade](docs/facade.md) summary.

## Usage

### Prerequisites

Executing these commands requires an installed version
of [Docker Compose](https://docs.docker.com/compose/install/).

### Running the application

    docker compose run --rm facade_app

<div align="center">
<img src="docs/facade_demo.gif" alt="drawing"/>
</div>

### Running the tests

    docker compose run --rm facade_app poetry run tests

## Development setup

The included project setting files can be used to continue development in PyCharm.

For a detailed description refer to [PyCharm project setup](docs/pycharm_project_setup.md).


